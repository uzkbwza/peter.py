import discord
import pickle
from discord.ext import commands
import logging

logging.basicConfig(level=logging.INFO)



def get_roles(roles):
    roles_list = [role for role in roles if "#[SFV]" in role.name]
    roles_dict = {role.name.split(" ")[1].lower(): role.id for role in roles_list}
    return([roles_list,roles_dict])

def get_colors(roles):
    roles_list = [role for role in roles if "[Color]" in role.name]
    roles_dict = {role.name: role.id for role in roles_list}
    return([roles_list,roles_dict])



class SFP():
    def __init__(self, bot):
        self.bot = bot
        self.rolefile = "roles"
        
        for server in self.bot.servers:
            print(server.id)
            print("{}: {}".format(server.name,server.id))
            if server.id == "404727290627620865":
                for emoji in server.emojis:
                    if emoji.name == "peter":
                        self.peter_emoji = emoji
        print(self.peter_emoji)


    # async def assign_color(self,role,user,message,colors_list):

    async def on_ready(self):
        open("roles","a+").close()
        open("colors","a+").close()

    async def on_message(self,message):
        # do some extra stuff here
        role_mentions = message.role_mentions
        user = message.author
        user_roles = user.roles
        print(len(message.reactions))
        # lmao no idea how else to do this ...
        '''color role stuff'''
        if len(role_mentions) > 0:
            print("YES!!!")
            colors_list = get_colors(message.server.roles)[0]
            colors_names = [role.name for role in colors_list]
            role = role_mentions[0]
            print([role])
            print(user_roles)
            if len(message.content.split(" ")) > 1: return
            if role in colors_list:
                print("role in colors list")
                print("checking user roles...")
                user_roles = user.roles
                for user_role in user_roles:
                    print(user_role)
                    if user_role in colors_list:
                        await self.bot.send_message(message.channel,"Sorry, due to a bug in Discord, you will have to use $$removecolor before I can assign you another one.")
                        return
                await self.bot.add_roles(user,role)
                if len(message.reactions) == 0:await self.bot.add_reaction(message,self.peter_emoji)
            print("color added")

    # @commands.command(pass_context=True)
    # async def emojis(self,ctx):
    #     for server in self.bot.servers:
    #         for emoji in server.emojis:
    #             print(emoji)

    @commands.command(pass_context=True)
    async def cr(self,ctx):
        """Checks and logs character roles of server. Users won't see any response from this message.
        """
        roles = get_roles(ctx.message.server.roles)
        roles_list = roles[0]
        roles_dict = roles[1]
        with open("roles","wb+") as file:
            pickle.dump(roles_dict,file)

        role_names = roles_dict.keys()
        for role_name in role_names:
            print(role_name +":   " + roles_dict[role_name])
        return([role_names,roles_list])

    @commands.command(pass_context=True)
    async def ccr(self,ctx):
        """Checks and logs color roles of server. Users won't see any response from this message.
        """
        roles = get_colors(ctx.message.server.roles)
        roles_list = roles[0]
        roles_dict = roles[1]
        with open("colors","wb+") as file:
            pickle.dump(roles_dict,file)

        role_names = roles_dict.keys()
        for role_name in role_names:
            print(role_name +":   " + roles_dict[role_name])
        return([role_names,roles_list])

    @commands.command(pass_context=True)
    async def chara(self,ctx,character):
        roles = get_roles(ctx.message.server.roles)            # if server.id == "404727290627620865":
            #     for emoji in server.emojis:
            #         if emoji.name == "peter":
            #             self.peter_emoji = emoji
        roles_list = roles[0]
        roles_dict = roles[1]
        role_names = roles_dict.keys()
        user = ctx.message.author
        user_roles = ctx.message.author.roles
        character = character.lower()

        if character == "peter":
            await self.bot.say("Sorry kiddo, there can be only one.")
            return

        if character not in role_names:
            await self.bot.say("Invalid character name.")
            return

        for role in roles_list:
            formatted_role_name = role.name.lower().replace("#[sfv] ","")
            if formatted_role_name == character:
                await self.bot.add_roles(user,role)
                if len(ctx.message.reactions) == 0: await self.bot.add_reaction(ctx.message,self.peter_emoji)
                print("Added new role.")
                return

    @commands.command(pass_context=True)
    async def removechara(self,ctx,character):
        roles = get_roles(ctx.message.server.roles)
        roles_list = roles[0]
        roles_dict = roles[1]
        role_names = roles_dict.keys()
        user = ctx.message.author
        user_roles = ctx.message.author.roles
        character = character.lower()

        if character not in role_names:
            await self.bot.say("Invalid character name.")
            return

        for role in roles_list:
            formatted_role_name = role.name.lower().replace("#[sfv] ","")
            if formatted_role_name == character:
                await self.bot.remove_roles(user,role)
                if len(ctx.message.reactions) == 0: await self.bot.add_reaction(ctx.message,self.peter_emoji)
                return

    @commands.command(pass_context=True)
    async def removecolor(self,ctx):
        roles = get_colors(ctx.message.server.roles)
        roles_list = roles[0]
        user = ctx.message.author
        user_roles = ctx.message.author.roles
        for role in roles_list:
            if role in user_roles:
                await self.bot.remove_roles(user,role)
                if len(ctx.message.reactions) == 0: await self.bot.add_reaction(ctx.message,self.peter_emoji)



def setup(bot):
    bot.add_cog(SFP(bot))