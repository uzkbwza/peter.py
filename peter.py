#!/usr/bin/python3
from discord.ext import commands
import asyncio
import logging
import os
import random
import pickle
import re
import sys
from time import sleep

# Init
##############
logging.basicConfig(level=logging.INFO)
description = '''SFP Mod bot'''
bot = commands.Bot(command_prefix='$$', description=description)
my_id = "97779349935116288"
startup_extensions = ["sfp"]
token = "NDEwMzEzMzk0OTI3ODI5MDAz.DVrVqg.FY5wqw_DxMi-gsP7Coxkc_fvQOw"
bot_testing = False
if bot_testing == True:
     token = "NDEwMjczOTQ2NjQzMjAyMDU5.DVqxrQ.dK9Qh11t4uYQ-jCZkDWL0ZD61q4"

@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')

@bot.command(pass_context=True)
async def load(ctx,extension_name : str,):
    if ctx.message.author.id != my_id:
        await bot.say("You are not permitted to use this command.")
        return
    """Loads an extension."""
    try:
        bot.load_extension(extension_name)
    except (AttributeError, ImportError) as e:
        await bot.say("```py\n{}: {}\n```".format(type(e).__name__, str(e)))
        return
    await bot.say("{} loaded. *cocks gun*".format(extension_name))

@bot.command(pass_context=True)
async def unload(ctx,extension_name : str):
    if ctx.message.author.id != my_id:
        await bot.say("You are not permitted to use this command.")
        return
    """Unloads an extension."""
    bot.unload_extension(extension_name)
    await bot.say("{} unloaded.".format(extension_name))

@bot.command(pass_context=True)
async def reload(ctx,extension_name : str,):
    if ctx.message.author.id != my_id:
        await bot.say("You are not permitted to use this command.")
        return
    """Reloads an extension."""
    bot.unload_extension(extension_name)

    try:
        bot.load_extension(extension_name)
    except (AttributeError, ImportError) as e:
        await bot.say("```py\n{}: {}\n```".format(type(e).__name__, str(e)))
        return
    await bot.say("{} reloaded. *cocks gun*".format(extension_name))

if __name__ == "__main__":
    for extension in startup_extensions:
        try:
            bot.load_extension(extension)
        except Exception as e:
            exc = '{}: {}'.format(type(e).__name__, e)
            print('Failed to load extension {}\n{}'.format(extension, exc))



bot.run(token)